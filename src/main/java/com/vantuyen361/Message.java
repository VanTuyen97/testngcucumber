package com.vantuyen361;

public class Message{
    private String message;
    private boolean isShowed = false;

    public Message(String message) {
        this.message = message;
    }

    public void show() {
        System.out.println(message);
        isShowed = true;
    }

    public boolean isShowed(){
        return isShowed;
    }
}