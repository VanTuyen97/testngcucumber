package com.vantuyen361.testcase;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"pretty"}, 
    features = "classpath:feature/Hello.feature", 
    glue = {"com.vantuyen361.step"},
    strict = true)
public class HelloTest extends AbstractTestNGCucumberTests{
}