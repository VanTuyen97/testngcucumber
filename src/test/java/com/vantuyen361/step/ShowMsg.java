package com.vantuyen361.step;

import com.vantuyen361.Message;

import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ShowMsg{
    Message message = null;

    @Given("message is {string}")
    public void setMessage(String msg) {
        message = new Message(msg);
    }

    @When("showed message")
    public void showMsg() {
        message.show();
    }

    @Then("message is showed")
    public void msgShowed() {
        Assert.assertTrue(message.isShowed());
    }
}